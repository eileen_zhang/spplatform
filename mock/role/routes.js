// Just a mock data

const constantRoutes = [
  {
    path: "/redirect",
    component: "layout/Layout",
    hidden: true,
    children: [
      {
        path: "/redirect/:path*",
        component: "views/redirect/index"
      }
    ]
  },
  {
    path: "/login",
    component: "views/login/index",
    hidden: true
  },
  {
    path: "/auth-redirect",
    component: "views/login/auth-redirect",
    hidden: true
  },
  {
    path: "/404",
    component: "views/error-page/404",
    hidden: true
  },
  {
    path: "/401",
    component: "views/error-page/401",
    hidden: true
  },
  {
    path: "",
    component: "layout/Layout",
    redirect: "dashboard",
    children: [
      {
        path: "dashboard",
        component: "views/dashboard/index",
        name: "Dashboard",
        meta: { title: "dashboard", icon: "dashboard", affix: true }
      }
    ]
  }
];

const asyncRoutes = [
  {
    path: "/article",
    name: "Article",
    component: "layout/Layout",
    prevent: true,
    order: 1,
    meta: {
      title: "article",
      icon: "el-icon-s-help",
      roles: ["admin", "editor"]
    },
    children: [
      {
        path: "/article/create",
        component: "modules/article/views/create",
        name: "CreateArticle",
        meta: { title: "createArticle", icon: "edit" },
        order: 1
      },
      {
        path: "/article/edit/:id(\\d+)",
        component: "modules/article/views/edit",
        name: "EditArticle",
        meta: {
          title: "editArticle",
          noCache: true,
          activeMenu: "/article/list"
        },
        hidden: true,
        order: 2
      },
      {
        path: "/article/list",
        component: "modules/article/views/list",
        name: "ArticleList",
        meta: { title: "articleList", icon: "list" },
        order: 3
      }
    ]
  },
  {
    path: "/example",
    name: "Example",
    component: "layout/Layout",
    prevent: true,
    order: 2,
    meta: {
      title: "example",
      icon: "el-icon-s-help"
    },
    children: [
      {
        path: "/example/charts",
        component: () => import("@/modules/example/views/index"),
        redirect: "/example/charts/keyboard",
        name: "Charts",
        meta: {
          title: "charts",
          icon: "chart"
        },
        children: [
          {
            path: "keyboard",
            component: () => import("@/modules/example/views/charts/keyboard"),
            name: "KeyboardChart",
            meta: { title: "keyboardChart", noCache: true }
          },
          {
            path: "line",
            component: () => import("@/modules/example/views/charts/line"),
            name: "LineChart",
            meta: { title: "lineChart", noCache: true }
          },
          {
            path: "mix-chart",
            component: () => import("@/modules/example/views/charts/mix-chart"),
            name: "MixChart",
            meta: { title: "mixChart", noCache: true }
          }
        ]
      },
      {
        path: "/example/clipboard",
        component: () => import("@/modules/example/views/index"),
        children: [
          {
            path: "index",
            component: () => import("@/modules/example/views/clipboard/index"),
            name: "ClipboardDemo",
            meta: { title: "clipboardDemo", icon: "clipboard" }
          }
        ]
      },
      {
        path: "/example/components",
        component: () => import("@/modules/example/views/index"),
        redirect: "noRedirect",
        name: "ComponentDemo",
        meta: {
          title: "components",
          icon: "component"
        },
        children: [
          {
            path: "tinymce",
            component: () =>
              import("@/modules/example/views/components-demo/tinymce"),
            name: "TinymceDemo",
            meta: { title: "tinymce" }
          },
          {
            path: "markdown",
            component: () =>
              import("@/modules/example/views/components-demo/markdown"),
            name: "MarkdownDemo",
            meta: { title: "markdown" }
          },
          {
            path: "json-editor",
            component: () =>
              import("@/modules/example/views/components-demo/json-editor"),
            name: "JsonEditorDemo",
            meta: { title: "jsonEditor" }
          },
          {
            path: "split-pane",
            component: () =>
              import("@/modules/example/views/components-demo/split-pane"),
            name: "SplitpaneDemo",
            meta: { title: "splitPane" }
          },
          {
            path: "avatar-upload",
            component: () =>
              import("@/modules/example/views/components-demo/avatar-upload"),
            name: "AvatarUploadDemo",
            meta: { title: "avatarUpload" }
          },
          {
            path: "dropzone",
            component: () =>
              import("@/modules/example/views/components-demo/dropzone"),
            name: "DropzoneDemo",
            meta: { title: "dropzone" }
          },
          {
            path: "sticky",
            component: () =>
              import("@/modules/example/views/components-demo/sticky"),
            name: "StickyDemo",
            meta: { title: "sticky" }
          },
          {
            path: "count-to",
            component: () =>
              import("@/modules/example/views/components-demo/count-to"),
            name: "CountToDemo",
            meta: { title: "countTo" }
          },
          {
            path: "mixin",
            component: () =>
              import("@/modules/example/views/components-demo/mixin"),
            name: "ComponentMixinDemo",
            meta: { title: "componentMixin" }
          },
          {
            path: "back-to-top",
            component: () =>
              import("@/modules/example/views/components-demo/back-to-top"),
            name: "BackToTopDemo",
            meta: { title: "backToTop" }
          },
          {
            path: "drag-dialog",
            component: () =>
              import("@/modules/example/views/components-demo/drag-dialog"),
            name: "DragDialogDemo",
            meta: { title: "dragDialog" }
          },
          {
            path: "drag-select",
            component: () =>
              import("@/modules/example/views/components-demo/drag-select"),
            name: "DragSelectDemo",
            meta: { title: "dragSelect" }
          },
          {
            path: "dnd-list",
            component: () =>
              import("@/modules/example/views/components-demo/dnd-list"),
            name: "DndListDemo",
            meta: { title: "dndList" }
          },
          {
            path: "drag-kanban",
            component: () =>
              import("@/modules/example/views/components-demo/drag-kanban"),
            name: "DragKanbanDemo",
            meta: { title: "dragKanban" }
          }
        ]
      },
      {
        path: "/example/error-log",
        component: () => import("@/modules/example/views/index"),
        children: [
          {
            path: "log",
            component: () => import("@/modules/example/views/error-log/index"),
            name: "ErrorLog",
            meta: { title: "errorLog", icon: "bug" }
          }
        ]
      },
      {
        path: "/example/error",
        component: () => import("@/modules/example/views/index"),
        redirect: "noRedirect",
        name: "ErrorPages",
        meta: {
          title: "errorPages",
          icon: "404"
        },
        children: [
          {
            path: "401",
            component: () => import("@/views/error-page/401"),
            name: "Page401",
            meta: { title: "page401", noCache: true }
          },
          {
            path: "404",
            component: () => import("@/views/error-page/404"),
            name: "Page404",
            meta: { title: "page404", noCache: true }
          }
        ]
      },
      {
        path: "/example/excel",
        component: () => import("@/modules/example/views/index"),
        redirect: "/example/excel/export-excel",
        name: "Excel",
        meta: {
          title: "excel",
          icon: "excel"
        },
        children: [
          {
            path: "export-excel",
            component: () =>
              import("@/modules/example/views/excel/export-excel"),
            name: "ExportExcel",
            meta: { title: "exportExcel" }
          },
          {
            path: "export-selected-excel",
            component: () =>
              import("@/modules/example/views/excel/select-excel"),
            name: "SelectExcel",
            meta: { title: "selectExcel" }
          },
          {
            path: "export-merge-header",
            component: () =>
              import("@/modules/example/views/excel/merge-header"),
            name: "MergeHeader",
            meta: { title: "mergeHeader" }
          },
          {
            path: "upload-excel",
            component: () =>
              import("@/modules/example/views/excel/upload-excel"),
            name: "UploadExcel",
            meta: { title: "uploadExcel" }
          }
        ]
      },
      {
        path: "/example/i18n",
        component: () => import("@/modules/example/views/index"),
        children: [
          {
            path: "index",
            component: () => import("@/modules/example/views/i18n-demo/index"),
            name: "I18n",
            meta: { title: "i18n", icon: "international" }
          }
        ]
      },
      {
        path: "/example/icon",
        component: () => import("@/modules/example/views/index"),
        children: [
          {
            path: "index",
            component: () => import("@/modules/example/views/icons/index"),
            name: "Icons",
            meta: { title: "icons", icon: "icon", noCache: true }
          }
        ]
      },
      {
        path: "/example/pdf",
        component: () => import("@/modules/example/views/index"),
        redirect: "/example/pdf/index",
        children: [
          {
            path: "index",
            component: () => import("@/modules/example/views/pdf/index"),
            name: "PDF",
            meta: { title: "pdf", icon: "pdf" }
          }
        ]
      },
      {
        path: "/example/pdf/download",
        component: () => import("@/modules/example/views/pdf/download"),
        hidden: true
      },
      {
        path: "/example/permission",
        component: () => import("@/modules/example/views/index"),
        redirect: "/example/permission/page",
        alwaysShow: true, // will always show the root menu
        name: "Permission",
        meta: {
          title: "permission",
          icon: "lock",
          roles: ["admin", "editor"] // you can set roles in root nav
        },
        children: [
          {
            path: "page",
            component: () => import("@/modules/example/views/permission/page"),
            name: "PagePermission",
            meta: {
              title: "pagePermission",
              roles: ["admin"] // or you can only set roles in sub nav
            }
          },
          {
            path: "directive",
            component: () =>
              import("@/modules/example/views/permission/directive"),
            name: "DirectivePermission",
            meta: {
              title: "directivePermission"
              // if do not set roles, means: this page does not require permission
            }
          },
          {
            path: "role",
            component: () => import("@/modules/example/views/permission/role"),
            name: "RolePermission",
            meta: {
              title: "rolePermission",
              roles: ["admin"]
            }
          }
        ]
      },
      {
        path: "/example/tab",
        component: () => import("@/modules/example/views/index"),
        children: [
          {
            path: "index",
            component: () => import("@/modules/example/views/tab/index"),
            name: "Tab",
            meta: { title: "tab", icon: "tab" }
          }
        ]
      },

      {
        path: "/example/table",
        component: () => import("@/modules/example/views/index"),
        redirect: "/table/complex-table",
        name: "Table",
        meta: {
          title: "Table",
          icon: "table"
        },
        children: [
          {
            path: "dynamic-table",
            component: () =>
              import("@/modules/example/views/table/dynamic-table/index"),
            name: "DynamicTable",
            meta: { title: "dynamicTable" }
          },
          {
            path: "drag-table",
            component: () => import("@/modules/example/views/table/drag-table"),
            name: "DragTable",
            meta: { title: "dragTable" }
          },
          {
            path: "inline-edit-table",
            component: () =>
              import("@/modules/example/views/table/inline-edit-table"),
            name: "InlineEditTable",
            meta: { title: "inlineEditTable" }
          },
          {
            path: "complex-table",
            component: () =>
              import("@/modules/example/views/table/complex-table"),
            name: "ComplexTable",
            meta: { title: "complexTable" }
          }
        ]
      },
      {
        path: "/example/theme",
        component: () => import("@/modules/example/views/index"),
        children: [
          {
            path: "index",
            component: () => import("@/modules/example/views/theme/index"),
            name: "Theme",
            meta: { title: "theme", icon: "theme" }
          }
        ]
      },
      {
        path: "/example/zip",
        component: () => import("@/modules/example/views/index"),
        redirect: "/example/zip/download",
        alwaysShow: true,
        name: "Zip",
        meta: { title: "zip", icon: "zip" },
        children: [
          {
            path: "download",
            component: () => import("@/modules/example/views/zip/index"),
            name: "ExportZip",
            meta: { title: "exportZip" }
          }
        ]
      },

      {
        path: "/example/external-link",
        component: () => import("@/modules/example/views/index"),
        children: [
          {
            path: "https://github.com/PanJiaChen/vue-element-admin",
            meta: { title: "externalLink", icon: "link" }
          }
        ]
      }
    ]
  },

  { path: "*", redirect: "/404", hidden: true }
];

module.exports = {
  constantRoutes,
  asyncRoutes
};
