module.exports = [
  // roles
  {
    url: "/BackendMgmtApi/api/Role/GetRoles",
    type: "get",
    response: config => {
      return {
        success: true,
        message: "Operate successfully",
        code: 90000,
        data: {
          total: 4,
          data: [
            {
              organizationId: 0,
              name: "Administrator111111",
              account: "admin",
              password: "e10adc3949ba59abbe56e057f20f883e",
              email: null,
              phone: null,
              id: 1,
              createdAt: "2021-03-09T12:33:58",
              createdBy: 1,
              modifiedAt: null,
              modifiedBy: null,
              isDeleted: false,
              deletedAt: null,
              deletedBy: null
            },
            {
              organizationId: 0,
              name: "1",
              account: "211213",
              password: null,
              email: "123",
              phone: null,
              id: 4,
              createdAt: "2021-03-04T16:53:45",
              createdBy: 1,
              modifiedAt: null,
              modifiedBy: null,
              isDeleted: false,
              deletedAt: null,
              deletedBy: null
            },
            {
              organizationId: 0,
              name: "adfs",
              account: "sadf",
              password: null,
              email: "fs",
              phone: "dfasf",
              id: 8,
              createdAt: "2021-03-04T18:52:52",
              createdBy: 1,
              modifiedAt: "2021-03-06T23:40:20",
              modifiedBy: 1,
              isDeleted: false,
              deletedAt: null,
              deletedBy: null
            },
            {
              organizationId: 0,
              name: "213111",
              account: "12312",
              password: null,
              email: "123",
              phone: "12312",
              id: 17,
              createdAt: "2021-03-08T14:08:37",
              createdBy: 1,
              modifiedAt: "2021-03-08T14:09:15",
              modifiedBy: 1,
              isDeleted: false,
              deletedAt: null,
              deletedBy: null
            }
          ]
        }
      };
    }
  }
];
