import Vue from "vue";
import Vuex from "vuex";
import getters from "./getters";

Vue.use(Vuex);

const modules = {};

const importStores = (storeModules, init) => {
  storeModules.forEach(store => {
    store.files.keys().reduce((module, modulePath) => {
      const moduleName = store.resolveName(modulePath);
      const value = store.files(modulePath);
      module[moduleName] = value.default;
      return module;
    }, init);
  });
};

const sharedStores = require.context("./shared", true, /\.js$/);

const moduleStores = require.context("../modules", true, /store\.js$/);

const moduleStores2 = require.context(
  "@/modules/",
  true,
  /\/store\/[a-zA-Z0-9]+\.js$/
);

const loadModules = () => {
  importStores(
    [
      {
        files: sharedStores,
        resolveName: modulePath => modulePath.replace(/^\.\/(.*)\.\w+$/, "$1") // set './app.js' => 'app'
      },
      {
        files: moduleStores2,
        resolveName: modulePath =>
          modulePath
            .substring(modulePath.lastIndexOf("/") + 1)
            .replace(/\.\w+$/, "") // set './app.js' => 'app'
      },
      {
        files: moduleStores,
        resolveName: modulePath =>
          modulePath &&
          modulePath
            .split("/")[1]
            .replace(/\-(\w)(\w+)/g, function(a, b, c) {
              return b.toUpperCase() + c.toLowerCase();
            })
            .replace("-", "") // set './article-test-2/store.js' => 'articleTest2'
      }
    ],
    modules
  );
};

loadModules();

const store = new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules,
  getters
});

// if (module.hot) {
//   module.hot.accept(sharedStores.id, () => {
//     loadSharedStores();

//     store.hotUpdate({
//       modules,
//     });
//   });

//   module.hot.accept(moduleStores.id, () => {
//     loadModulesStores();

//     store.hotUpdate({
//       modules,
//     });
//   });
// }

export default store;
