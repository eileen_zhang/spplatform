import { asyncRoutes, constantRoutes } from "@/router/router-data";
import { sortArr } from "@/utils";

/**
 * Use meta.role to determine if the current user has permission
 * @param roles
 * @param route
 */
function hasPermission(permissions, route) {
  // if (route.meta && route.meta.roles) {
  return permissions.some(permission => route.resourceId === permission.resourceId);
  // } else {
  //   return true;
  // }
}

/**
 * Filter asynchronous routing tables by recursion
 * @param routes asyncRoutes
 * @param roles
 */
export function filterAsyncRoutes(routes, roles) {
  const res = [];

  routes.forEach(route => {
    const tmp = { ...route };
    if (hasPermission(roles, tmp)) {
      if (tmp.children) {
        tmp.children = filterAsyncRoutes(tmp.children, roles);
      }
      res.push(tmp);
    }
  });

  return res;
}

/**
 * Filter permissionRoutes routing tables by show on menu
 * @param routes permissionRoutes
 * @param roles
 */
export function filterMenuRoutes(routes, permissions) {
  const res = [];
  const sortedRoutes = sortArr(routes, "order");
  sortedRoutes.forEach(route => {
    const tmp = { ...route };
    if (hasPermission(permissions, tmp)) {
      tmp.title = tmp.meta.title || tmp.name;
      if (tmp.children) {
        tmp.children = filterMenuRoutes(permissions, tmp.children);
      }
      res.push(tmp);
    }
  });
  return res;
}

const state = {
  routes: [],
  addRoutes: []
};

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes;
    const stortedConstRoute = sortArr(constantRoutes, "order");
    state.routes = stortedConstRoute.concat(routes);
  }
};

const actions = {
  generateRoutes({ commit }, data) {
    return new Promise(resolve => {
      let accessedRoutes;
      if (data.roles.includes(1)) {
        accessedRoutes = asyncRoutes || [];
      } else {
        accessedRoutes = filterMenuRoutes(asyncRoutes, data.permissions);
      }
      commit("SET_ROUTES", accessedRoutes);

      resolve(accessedRoutes);
    });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
