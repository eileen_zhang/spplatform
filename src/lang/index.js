import Vue from "vue";
import VueI18n from "vue-i18n";
import Cookies from "js-cookie";
import elementEnLocale from "element-ui/lib/locale/lang/en"; // element-ui lang
import elementZhLocale from "element-ui/lib/locale/lang/zh-CN"; // element-ui lang
import elementEsLocale from "element-ui/lib/locale/lang/es"; // element-ui lang
import elementJaLocale from "element-ui/lib/locale/lang/ja"; // element-ui lang

const locales = [
  {
    files: require.context("./en", true, /\.js$/),
    key: "en"
  },
  {
    files: require.context("./zh", true, /\.js$/),
    key: "zh"
  },
  {
    files: require.context("./es", true, /\.js$/),
    key: "es"
  },
  {
    files: require.context("./ja", true, /\.js$/),
    key: "ja"
  }
];

Vue.use(VueI18n);

const allLocale = {};

const importLocale = (locales, init) => {
  locales.forEach(locale => {
    var values = {};
    locale.files.keys().reduce((module, modulePath) => {
      const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, "$1");
      const value = locale.files(modulePath);
      module[moduleName] = value.default;
      return module;
    }, values);
    init[locale.key] = values;
  }, init);
};

importLocale(locales, allLocale);

const messages = {
  en: {
    ...allLocale.en,
    ...elementEnLocale
  },
  zh: {
    ...allLocale.zh,
    ...elementZhLocale
  },
  es: {
    ...allLocale.es,
    ...elementEsLocale
  },
  ja: {
    ...allLocale.ja,
    ...elementJaLocale
  }
};
export function getLanguage() {
  const chooseLanguage = Cookies.get("language");
  if (chooseLanguage) return chooseLanguage;

  // if has not choose language
  const language = (
    navigator.language || navigator.browserLanguage
  ).toLowerCase();
  const locales = Object.keys(messages);
  for (const locale of locales) {
    if (language.indexOf(locale) > -1) {
      return locale;
    }
  }
  return "en";
}
const i18n = new VueI18n({
  // set locale
  // options: en | zh | es
  locale: getLanguage(),
  // set locale messages
  messages
});

export default i18n;
