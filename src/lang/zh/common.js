export default {
  new: "新增",
  edit: "编辑",
  delete: "删除",
  update: "更新",
  modify: "修改",
  confirm: "确认",
  cancel: "取消",
  action: "操作",
  tip: "提示",
  warning: "警告",
  save: "保存",
  filterKeyWords: "输入关键字进行过滤",
  saveSuccess: "保存成功",
  deleteSuccess: "删除成功"
};
