export default {
  addRole: "Nuevo rol",
  editPermission: "Permiso de edición",
  roles: "Tus permisos",
  switchRoles: "Cambiar permisos",
  tips:
    "In some cases it is not suitable to use v-permission, such as element Tab component or el-table-column and other asynchronous rendering dom cases which can only be achieved by manually setting the v-if.",
  delete: "Borrar",
  confirm: "Confirmar",
  cancel: "Cancelar"
};
