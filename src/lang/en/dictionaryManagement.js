export default {
  number: "number",
  name: "name",
  code: "code",
  expandAll: "Expand All",
  collapseAll: "Collapse All",
  order: "order"
};
