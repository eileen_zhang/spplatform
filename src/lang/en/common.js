export default {
  new: "New",
  edit: "Edit",
  delete: "Delete",
  update: "Update",
  modify: "Modify",
  confirm: "Confirm",
  cancel: "Cancel",
  action: "Action",
  tip: "Tip",
  warning: "Warning",
  save: "Save",
  filterKeyWords: "please input filter keywords",
  saveSuccess: "Save succssfully",
  deleteSuccess: "Delete Successfully"
};
