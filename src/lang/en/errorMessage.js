export default {
  90000: "Operate successfully!",
  99999: "System exception!",
  90999: "Operate failed!",
  10001: "username and password can not be empty!",
  10002: "username or password is not correct!",
  10003: "get token failed!",
  10004: "not authentication",
  10005: "username changed failed",
  10006: "username already exist",
  10007: "Id is invalid!",
  10008: "role name already exist "
};
