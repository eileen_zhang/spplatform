export default {
  title: "Login Form",
  logIn: "Login",
  username: "Username",
  password: "Password",
  any: "any",
  thirdparty: "Or connect with",
  thirdpartyTips:
    "Can not be simulated on local, so please combine you own business simulation! ! !",
  reloginTitle: "Confirm logout",
  reloginText:
    "You have been logged out, you can cancel to stay on this page, or log in again",
  reloginOk: "Re-Login",
  reloginCancel: "Cancel"
};
