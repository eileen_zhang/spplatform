export default {
  addRole: "New Role",
  editPermission: "Edit",
  roles: "Your roles",
  switchRoles: "Switch roles",
  tips:
    "In some cases, using v-permission will have no effect. For example: Element-UI  el-tab or el-table-column and other scenes that dynamically render dom. You can only do this with v-if.",
  delete: "Delete",
  confirm: "Confirm",
  cancel: "Cancel"
};
