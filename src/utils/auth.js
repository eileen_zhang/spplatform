import Cookies from "js-cookie";

const TokenKey = "Admin-Token";

export function getToken() {
  return Cookies.get(TokenKey);
}

export function setToken(token) {
  return Cookies.set(TokenKey, token);
}

export function removeToken() {
  return Cookies.remove(TokenKey);
}

export function setAuthorization(Authorization) {
  return Cookies.set("Authorization", Authorization);
}

export function getAuthorization() {
  return Cookies.get("Authorization");
}

export function removeAuthorization() {
  return Cookies.remove("Authorization");
}
