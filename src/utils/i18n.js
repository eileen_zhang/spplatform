import i18n from "@/lang";

// translate router.meta.title, be used in breadcrumb sidebar tagsview
export function generateTitle(title) {
  const hasKey = this.$te("route." + title);

  if (hasKey) {
    // $t :this method from vue-i18n, inject in @/lang/index.js
    const translatedTitle = this.$t("route." + title);

    return translatedTitle;
  }
  return title;
}

// translate error message
export function showErrorMessage(error) {
  const hasKey = i18n.te("errorMessage." + error.code);
  if (hasKey) {
    // $t :this method from vue-i18n, inject in @/lang/index.js
    const translatedMessage = i18n.t("errorMessage." + error.code);
    return translatedMessage;
  }
  return error.Message;
}

export function translate(key) {
  const hasKey = i18n.te(key);

  if (hasKey) {
    // $t :this method from vue-i18n, inject in @/lang/index.js
    const translatedMessage = i18n.t(key);
    return translatedMessage;
  }
  return key;
}
