import axios from "axios";
import { MessageBox, Message } from "element-ui";
import store from "@/store";
import { getToken, getAuthorization } from "@/utils/auth";
import { showErrorMessage, translate } from "@/utils/i18n";
import { encrypt } from "@/utils/encrypt";

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 10000 // request timeout
});

// request interceptor
service.interceptors.request.use(
  config => {
    console.log(config);
    if (config.data && process.env.NODE_ENV === "production") {
      // const dataString = JSON.stringify(config.data);
      // console.log({ param: encrypt(dataString) });
      // config.data = { param: encrypt(dataString) };
    }
    // do something before request is sent
    config.headers["Authorization"] = "Bearer " + getAuthorization();
    if (store.getters.token) {
      // let each request carry token
      // ['X-Token'] is a custom headers key
      // please modify it according to the actual situation
      config.headers["X-Token"] = getToken();
    }
    return config;
  },
  error => {
    // do something with request error
    console.log(error); // for debug
    return Promise.reject(error);
  }
);

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data;

    // if the custom code is not 20000, it is judged as an error.
    if (res.success !== true) {
      // 10003: Illegal token; 10004: Other clients logged in; 50014: Token expired;
      if (res.code === 10003 || res.code === 10004) {
        // to re-login
        MessageBox.confirm(translate("login.reloginText"), translate("login.reloginTitle"), {
          confirmButtonText: translate("login.reloginOk"),
          cancelButtonText: translate("login.reloginCancel"),
          type: "warning"
        }).then(() => {
          store.dispatch("user/resetToken").then(() => {
            location.reload();
          });
        });
      } else {
        // Message({
        //   message: showErrorMessage(res) || "Upexpected errors occured.",
        //   type: "error",
        //   duration: 5 * 1000
        // });
        MessageBox.alert(showErrorMessage(res) || "Upexpected errors occured.", {
          confirmButtonText: translate("common.confirm"),
          type: "error"
        });
      }
      return res;
      // return Promise.reject(
      //   new Error(res.message || "Upexpected errors occured.")
      // );
    } else {
      return res;
    }
  },
  error => {
    console.log("err" + error); // for debug
    Message({
      message: error.message,
      type: "error",
      duration: 5 * 1000
    });
    return Promise.reject(error);
  }
);

export default service;
