import JSEncrypt from "jsencrypt";

export function encrypt(str) {
  const pubKey = `-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC/GEUbq9prMWEiM4SS5fvONFXU
mbYAVqXKG1D3DGYneGdp/H4nxZlJoh28AvgW9FuhFF0cSkKzkh5vBI6N5LjDU9r1
MmzDfnyjNvWWEKF2wUhiftvsj6cvyrNXG6aktEGJt/i0Ks2XvQlOhNzuA5xFX/np
o8gL+JfZJAmMfBrfaQIDAQAB
-----END PUBLIC KEY-----`;
  const encryptStr = new JSEncrypt();
  encryptStr.setPublicKey(pubKey);
  const data = encryptStr.encrypt(str.toString());
  return data;
}
