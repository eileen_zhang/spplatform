// import Home from "../views/Home.vue";
import Layout from "@/layout";
// import Login from "../views/Login";
// import Page404 from "../views/404";
// import Page403 from "../views/403";
// import Page500 from "../views/500";
// import About from "../views/About";

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

export const constantRoutes = [
  {
    path: "/redirect",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "/redirect/:path(.*)",
        component: () => import("@/views/redirect/index")
      }
    ]
  },
  {
    path: "/login",
    component: () => import("@/views/login/index"),
    hidden: true
  },
  {
    path: "/auth-redirect",
    component: () => import("@/views/login/auth-redirect"),
    hidden: true
  },
  {
    path: "/send-reset-request",
    component: () => import("@/views/reset-password/send-reset-request"),
    hidden: true
  },
  {
    path: "/reset-password",
    component: () => import("@/views/reset-password/reset-password"),
    hidden: true
  },
  {
    path: "/404",
    component: () => import("@/views/error-page/404"),
    hidden: true
  },
  {
    path: "/401",
    component: () => import("@/views/error-page/401"),
    hidden: true
  },
  {
    path: "/",
    component: Layout,
    redirect: "/dashboard",
    children: [
      {
        path: "dashboard",
        component: () => import("@/views/dashboard/index"),
        name: "Dashboard",
        meta: { title: "dashboard", icon: "dashboard", affix: true }
      }
    ]
  },
  {
    path: "/profile",
    component: Layout,
    redirect: "/profile/index",
    hidden: true,
    children: [
      {
        path: "index",
        component: () => import("@/views/profile/index"),
        name: "Profile",
        meta: { title: "profile", icon: "user", noCache: true }
      }
    ]
  },
  {
    path: "/error",
    component: Layout,
    redirect: "noRedirect",
    name: "ErrorPages",
    hidden: true,
    meta: {
      title: "errorPages",
      icon: "404"
    },
    children: [
      {
        path: "/error/401",
        component: () => import("@/views/error-page/401"),
        name: "Page401",
        meta: { title: "page401", noCache: true }
      },
      {
        path: "/error/404",
        component: () => import("@/views/error-page/404"),
        name: "Page404",
        meta: { title: "page404", noCache: true }
      }
    ]
  }
];

const moduleRoutes = require.context("../modules", true, /router\.js$/);
const importRoutes = () => {
  const modules = moduleRoutes.keys().reduce((module, modulePath) => {
    const value = moduleRoutes(modulePath);
    module = module.concat(value.routes);
    return module;
  }, []);

  modules.push({ path: "*", redirect: "/error/404", hidden: true });
  return modules;
};

export const asyncRoutes = importRoutes();
