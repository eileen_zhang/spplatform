import request from "@/utils/request";
import md5 from "js-md5";

export function login(data) {
  const password = md5(data.password);
  const account = data.username;
  return request({
    url: "IdentityServer/api/Auth/BackendAuth",
    method: "post",
    data: { account, password }
  });
}

export function getInfo() {
  return request({
    url: "/BackendMgmtApi/api/User/GetCurrentUser",
    method: "get"
  });
}

export function logout(token) {
  return request({
    url: "/IdentityServer/api/Auth/logout",
    method: "post",
    data: { token }
  });
}

export function getAllPermission(roles) {
  return request({
    url: "/BackendMgmtApi/api/Permission/GetAllPermission",
    method: "get",
    params: roles
  });
}

export function sendResetPasswordEmail(authEmail) {
  return request({
    url: "/IdentityServer/api/Auth/SendResetMail",
    method: "post",
    data: { authEmail }
  });
}

export function verifyEmailToken(data) {
  return request({
    url: "/IdentityServer/api/Auth/VerifyEmailToken",
    method: "get",
    params: data
  });
}

export function UpdatePasswordByEmailToken(data) {
  return request({
    url: "/IdentityServer/api/Auth/UpdatePasswordByEmailToken",
    method: "post",
    data
  });
}
