import request from "@/utils/request";

export function getTreeDictionaries() {
  return request({
    url: "api/Dictionary/GetTreeDictionaries",
    method: "get"
  });
}

// Updates the category or dictionary name.
export function updateCategory(data) {
  return request({
    url: "api/Dictionary/UpdateCategory",
    method: "post",
    data
  });
}

export function addSubDictionary(data) {
  return request({
    url: "api/Dictionary/AddDictionary",
    method: "post",
    data
  });
}

export function updateSubDictionary(data) {
  return request({
    url: "api/Dictionary/UpdateDictionary",
    method: "post",
    data
  });
}

export function deleteSubDictionary(data) {
  return request({
    url: "api/Dictionary/DeleteDictionary",
    method: "post",
    data
  });
}
