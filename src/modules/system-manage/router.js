/* Layout */
import Layout from "@/layout";

export const routes = [
  {
    resourceId: "systemManageMenu",
    path: "/system-manage",
    name: "SystemManage",
    component: Layout,
    prevent: true,
    order: 3,
    meta: {
      title: "SystemManage",
      icon: "el-icon-s-help",
      roles: ["admin", "editor"]
    },
    isReadonly: false,
    isActionable: false,
    children: [
      {
        resourceId: "userManageMenu",
        path: "user-manage",
        component: () => import("@/modules/system-manage/views/user-manage"),
        name: "UserManage",
        meta: { title: "UserManage", icon: "edit" },
        order: 1,
        isReadonly: false,
        isActionable: false
      },
      {
        resourceId: "roleManageMenu",
        path: "role-manage",
        component: () => import("@/modules/system-manage/views/role-manage"),
        name: "RoleManage",
        meta: { title: "RoleManage", icon: "list" },
        order: 2,
        isReadonly: false,
        isActionable: false
      },
      {
        resourceId: "menuManageMenu",
        path: "menu-manage",
        component: () => import("@/modules/system-manage/views/menu-manage"),
        name: "MenuManage",
        meta: { title: "MenuManage", icon: "list" },
        order: 4,
        isReadonly: false,
        isActionable: false
      },
      {
        resourceId: "dictionaryManageMenu",
        path: "dictionary-manage",
        component: () => import("@/modules/system-manage/views/dictionary-manage"),
        name: "DictionaryManage",
        meta: { title: "DictionaryManage", icon: "list" },
        order: 5,
        isReadonly: false,
        isActionable: false
      }
    ]
  }
];
