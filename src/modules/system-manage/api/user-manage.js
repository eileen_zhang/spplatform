import request from "@/utils/request";
import md5 from "js-md5";

export function getList(query) {
  return request({
    url: "/BackendMgmtApi/api/User/GetUsers",
    method: "get",
    params: query
  });
}

export function addUser(data) {
  data.password = md5(data.password);
  return request({
    url: "/BackendMgmtApi/api/User/SaveUser",
    method: "post",
    data
  });
}

export function deleteUser(userId) {
  return request({
    url: "/BackendMgmtApi/api/User/DeleteUser",
    method: "post",
    params: { userId }
  });
}

export function getUser(userId) {
  return request({
    url: "/BackendMgmtApi/api/User/GetUserById",
    method: "get",
    params: { userId }
  });
}

export function updateUser(data) {
  return request({
    url: "/BackendMgmtApi/api/User/SaveUser",
    method: "post",
    data
  });
}

export function getRoleList(query) {
  return request({
    url: "/BackendMgmtApi/api/Role/GetRoles",
    method: "get",
    params: query
  });
}
