import request from "@/utils/request";

export function getList(query) {
  return request({
    url: "/BackendMgmtApi/api/Role/GetRoles",
    method: "get",
    params: query
  });
}

export function addRole(data) {
  return request({
    url: "/BackendMgmtApi/api/role/addRole",
    method: "post",
    data
  });
}

export function deleteRole(roleId) {
  return request({
    url: "/BackendMgmtApi/api/role/DeleteRole",
    method: "post",
    params: { roleId }
  });
}

export function getRole(roleId) {
  return request({
    url: "/BackendMgmtApi/api/role/GetRole",
    method: "get",
    params: { roleId }
  });
}

export function updateRole(data) {
  return request({
    url: "/BackendMgmtApi/api/role/UpdateRole",
    method: "post",
    data
  });
}
