import request from "@/utils/request";

export function savePermission(data) {
  return request({
    url: "/BackendMgmtApi/api/Permission/SavePermission",
    method: "post",
    data
  });
}

export function getPermission(roleId) {
  return request({
    url: "/BackendMgmtApi/api/Permission/GetPermissionByRoleId",
    method: "get",
    params: { roleId }
  });
}
