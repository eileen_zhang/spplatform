/* Layout */
import Layout from "@/layout";

export const routes = [
  {
    resourceId: "articlemenu",
    path: "/article",
    name: "Article",
    component: Layout,
    redirect: "/article/list",
    prevent: true,
    order: 1,
    isReadonly: false,
    isActionable: false,
    meta: {
      title: "Article",
      icon: "el-icon-s-help",
      roles: ["admin", "editor"]
    },
    children: [
      {
        resourceId: "createArticleMenu",
        path: "create",
        component: () => import("@/modules/article/views/create"),
        name: "CreateArticle",
        meta: { title: "createArticle", icon: "edit" },
        order: 1,
        isReadonly: false,
        isActionable: false
      },
      {
        resourceId: "editArticleMenu",
        path: "edit/:id(\\d+)",
        component: () => import("@/modules/article/views/edit"),
        name: "EditArticle",
        meta: {
          title: "EditArticle",
          noCache: true,
          activeMenu: "/article/list"
        },
        order: 2,
        isReadonly: false,
        isActionable: false
      },
      {
        resourceId: "articleListMenu",
        path: "list",
        component: () => import("@/modules/article/views/list"),
        name: "ArticleList",
        meta: { title: "articleList", icon: "list" },
        order: 3,
        isReadonly: false,
        isActionable: false
      }
    ]
  }
];
